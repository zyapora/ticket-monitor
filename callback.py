"""define callbacks to LINE."""

# import external modules
from sys import stderr
from flask import Blueprint, request, abort
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError, LineBotApiError
from linebot.models import MessageEvent, TextMessage, TextSendMessage

# import internal modules
from models import db
from controllers.message_handler import MessageHandler
from properties import line_property, mone_error

# init line api interface, line post handler
line_bot_api = LineBotApi(line_property.YOUR_CHANNEL_ACCESS_TOKEN)
line_handler = WebhookHandler(line_property.YOUR_CHANNEL_SECRET)

app = Blueprint('callback', __name__)
handler = MessageHandler(db)


@app.route('', methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']
    body = request.get_data(as_text=True)
    try:
        line_handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'


@line_handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    res = handler.handle_message(event.message.text)
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=res))


@line_handler.default()
def default(event):
    stderr.write(mone_error.UNKNOWN_EVENT_MESSAGE)
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=mone_error.UNKNOWN_EVENT_MESSAGE))
