import re

from models import Event
from properties import message_rules


class EventGenerator:
    def __init__(self):
        pass

    def generate_event(self, message):
        shop, day, title = parse_full_query(message)
        return Event(shop, day, title)


def parse_full_query(message):
    tokens = re.split(message_rules.MESSAGE_SEPARATOR_REGEX, message)
    return tokens[0], tokens[1], tokens[2]
