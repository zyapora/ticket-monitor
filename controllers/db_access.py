from models import Event
from controllers import req_trigger


class DBAccess:
    def __init__(self, db):
        self.db = db

    def add_event(self, event):
        self.db.session.add(event)
        self.db.session.commit()
        return 'posted\n' + str(event)

    def delete_all_events(self):
        for event in Event.query.all():
            self.db.session.delete(event)
        self.db.session.commit()
        return 'deleted all events'

    def get_full(self):
        ev = Event.query.order_by(Event.id.desc()).first()
        rest_res = req_trigger.get_event_list(ev.shop_id,
                                              ev.event_date,
                                              ev.content_code)
        return str(rest_res)

    def get_first(self):
        ev = Event.query.order_by(Event.id.desc()).first()
        rest_res = req_trigger.get_first_event(ev.shop_id,
                                               ev.event_date,
                                               ev.content_code)
        return str(rest_res)

    def get_stock(self):
        ev = Event.query.order_by(Event.id.desc()).first()
        rest_res = req_trigger.get_event_list(ev.shop_id,
                                              ev.event_date,
                                              ev.content_code)
        res = str(ev) + '\n\n' + get_eachtime_stock(rest_res)
        return res

    def get_count(self):
        res = str(len(Event.query.all()))
        return res

    def get_info(self):
        res = str(Event.query.order_by(Event.id.desc()).first())
        return res

    def get_all_info(self):
        all_event = Event.query.all()
        res = ''
        for event in all_event:
            res += str(event) + '\n\n'
        return str(res)

    def get_all_post(self):
        all_event = Event.query.all()
        res = ''
        for event in all_event:
            res += event.get_post_str() + '\n\n'
        return str(res)


def get_eachtime_stock(rest_res):
    time_list = rest_res['events']
    res = ''
    for a_time in time_list:
        res += a_time['start_time'] + ' : ' + a_time['ticket_stock'] + '\n'

    return res
