import requests
from properties import scrap_property


def build_api_url(shop_id, event_date, content_code):
    url = scrap_property.BASE_URL + '?'
    url += 'shop_id=' + shop_id
    url += '&event_date=' + event_date
    url += '&content_code=' + content_code
    return url


def get_event_list(shop_id, event_date, content_code):
    response = requests.get(
        build_api_url(shop_id, event_date, content_code)
    ).json()
    res = response['contents'][0]
    return res


def get_first_event(shop_id, event_date, content_code):
    response = requests.get(
        build_api_url(shop_id, event_date, content_code)
    ).json()
    res = response['contents'][0]['events'][0]
    return res
