from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Event(db.Model):
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    shop_id = db.Column(db.String(80), unique=False)
    event_date = db.Column(db.String(80), unique=False)
    content_code = db.Column(db.String(80), unique=False)

    def __init__(self, shop_id, event_date, content_code):
        self.shop_id = shop_id
        self.event_date = event_date
        self.content_code = content_code

    def __repr__(self):
        return '<Event %r>' % self.id

    def __str__(self):
        return 'shop: ' + self.shop_id + \
            '\ndate: ' + self.event_date + \
            '\ncode: ' + self.content_code

    def get_post_str(self):
        return self.shop_id + \
            '\n' + self.event_date + \
            '\n' + self.content_code
