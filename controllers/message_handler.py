import re

from controllers.db_access import DBAccess
from controllers.event_generator import EventGenerator
from properties import mone_error, message_rules


class MessageHandler:
    def __init__(self, db):
        self.db_acc = DBAccess(db)
        self.event_gen = EventGenerator()

    def handle_message(self, message):
        res = mone_error.UNKNOWN_POST_MESSAGE
        command, optional = parse_command(message)
        if (command == 'add'):
            res = self.db_acc.add_event(
                self.event_gen.generate_event(optional))
        elif (command == 'first'):
            res = self.db_acc.get_first()
        # elif (command == 'full'):
        #     res = self.db_acc.get_full()
        elif (command == 'stock'):
            res = self.db_acc.get_stock()
        elif (command == 'count'):
            res = self.db_acc.get_count()
        elif (command == 'info'):
            res = self.db_acc.get_info()
        elif (command == 'all'):
            res = self.db_acc.get_all_info()
        elif (command == 'allpost'):
            res = self.db_acc.get_all_post()
        elif (command == 'del'):
            res = self.db_acc.delete_all_events()
        else:
            pass
        return res


def parse_command(message):
    tokens = re.split(message_rules.MESSAGE_SEPARATOR_REGEX, message, 1)
    if (len(tokens) == 2):
        return tokens[0], tokens[1]
    elif (len(tokens) == 1):
        return tokens[0], ''
    else:
        return '', ''
