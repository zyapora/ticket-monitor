import os

from flask import Flask
from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand

from models import db

# create flask app instance
app = Flask(__name__)

# db init
DATABASE_URL = 'sqlite:///'
try:
    from local_database import DATABASE_URL
except ImportError:
    DATABASE_URL = os.environ['DATABASE_URL']
    pass
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_NATIVE_UNICODE'] = 'utf-8'
app.config['DEBUG'] = True
db.init_app(app)
db.app = app

# add blueprints
import callback
app.register_blueprint(callback.app, url_prefix="/callback")

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('runserver', Server(host='0.0.0.0',
                    port=int(os.environ.get("PORT", 80))))

# count = req_trigger.get_event_list('101', '2018/06/16', 'Dracula')
if __name__ == '__main__':
    manager.run()
